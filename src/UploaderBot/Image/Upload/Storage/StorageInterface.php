<?php
declare(strict_types = 1);

namespace UploaderBot\Image\Upload\Storage;

interface StorageInterface
{
    /**
     * @param \SplFileInfo $fileInfo
     * @return bool
     */
    public function upload(\SplFileInfo $fileInfo): bool;
}
