<?php
declare(strict_types = 1);

namespace UploaderBot\Image\Upload\Storage;

use Dropbox;

class DropboxStorage implements StorageInterface
{
    /**
     * @var string
     */
    private $key;
    /**
     * @var string
     */
    private $secret;
    /**
     * @var string
     */
    private $token;

    /**
     * DropboxStorage constructor.
     * @param string $key
     * @param string $secret
     * @param string $token
     */
    public function __construct(string $key, string $secret, string $token)
    {

        $this->key = $key;
        $this->secret = $secret;
        $this->token = $token;
    }

    /**
     * @inheritdoc
     */
    public function upload(\SplFileInfo $fileInfo): bool
    {
        $dbxClient = new Dropbox\Client($this->token, "PHP-Example/1.0");
        
        $f = fopen($fileInfo->getRealPath(), "rb");
        $result = $dbxClient->uploadFile("/".$fileInfo->getBasename(), Dropbox\WriteMode::add(), $f);
        fclose($f);

        if (is_array($result)) {
            return true;
        }

        return false;
    }
}
