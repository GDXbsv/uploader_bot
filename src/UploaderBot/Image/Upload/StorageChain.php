<?php
declare(strict_types = 1);

namespace UploaderBot\Image\Upload;


use UploaderBot\Image\Upload\Storage\StorageInterface;

class StorageChain
{
    /** @var array */
    private $storageList = [];

    public function addStorage(string $alias, StorageInterface $storage): StorageChain
    {
        $this->storageList[$alias] = $storage;

        return $this;
    }

    public function getStorage(string $storageName): StorageInterface
    {
        if (!array_key_exists($storageName, $this->storageList)) {
            throw new \InvalidArgumentException(
                sprintf(
                    'Storage "%s" is not correct. Available: "%s"',
                    $storageName,
                    implode(',', array_keys($this->storageList))
                )
            );
        }

        return $this->storageList[$storageName];
    }
}
