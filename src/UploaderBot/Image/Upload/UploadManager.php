<?php
declare(strict_types = 1);

namespace UploaderBot\Image\Upload;


class UploadManager
{
    /**
     * @var StorageChain
     */
    private $storageChain;

    /**
     * Uploader constructor.
     * @param StorageChain $storageChain
     */
    public function __construct(StorageChain $storageChain)
    {
        $this->storageChain = $storageChain;
    }

    /**
     * @param string $storageName
     * @param \SplFileInfo $fileInfo
     * @return bool
     */
    public function upload(string $storageName, \SplFileInfo $fileInfo): bool
    {
        $storage = $this->storageChain->getStorage($storageName);

        return $storage->upload($fileInfo);
    }

}
