<?php
declare(strict_types = 1);

namespace UploaderBot\Image\Resize;

class ResizeManager
{
    /**
     * @var int
     */
    private $filterType;
    /**
     * @var int
     */
    private $blur;
    /**
     * @var string
     */
    private $tempResizePath;

    /**
     * Resizer constructor.
     * @param string $tempResizePath
     * @param int $filterType
     * @param int $blur
     */
    public function __construct(string $tempResizePath, int $filterType = \Imagick::FILTER_LANCZOS, int $blur = 1)
    {
        $this->filterType = $filterType;
        $this->blur = $blur;
        $this->tempResizePath = $tempResizePath;
    }

    /**
     * @param \SplFileInfo $imagePath
     * @return \SplFileInfo
     */
    public function resizeAndDrop(\SplFileInfo $imagePath): \SplFileInfo
    {
        $imageResultDir = $this->tempResizePath;
        if ($imageResultDir[0] !== '/') {
            $imageResultDir = getcwd() . '/' . $imageResultDir;
        }
        if (!file_exists($imageResultDir)) {
            mkdir($imageResultDir, 0777, true);
        }
        $imageResultPath = new \SplFileInfo($imageResultDir.'/'.$imagePath->getBasename());

        $this->resizeImage($imagePath, $imageResultPath, 640, 640);

        if ($imagePath->getRealPath() !== $imageResultPath->getRealPath()) {
            $isDrop = unlink($imagePath->getPathname());
            if (!$isDrop) {
                throw new \LogicException(sprintf('It does not delete file %s', $imagePath->getPathname()));
            }
        }

        return $imageResultPath;
    }

    /**
     * @param \SplFileInfo $imagePath
     * @param \SplFileInfo $imageResultPath
     * @param $width
     * @param $height
     */
    public function resizeImage(\SplFileInfo $imagePath, \SplFileInfo $imageResultPath, $width, $height)
    {

        $thumb = new \Imagick($imagePath->getPathname());

        $thumb->resizeImage($width, $height, $this->filterType, $this->blur);
        $thumb->writeImage($imageResultPath->getPathname());

        $thumb->destroy();
    }
}
