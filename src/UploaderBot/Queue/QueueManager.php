<?php
declare(strict_types = 1);

namespace UploaderBot\Queue;

use UploaderBot\Queue\Message\MessageInterface;

class QueueManager
{
    /** @var \PDO */
    private $pdo;
    /**
     * @var string
     */
    private $sqliteDb;

    /**
     * QueueManager constructor.
     * @param string $sqliteDb
     */
    public function __construct(string $sqliteDb)
    {
        $this->sqliteDb = $sqliteDb;
        $this->queue = $this->getBaseQueue();
    }

    /**
     * Configure database
     *
     * @throws \Exception
     * @return \PDO
     */
    public function getDb(): \PDO
    {
        if (!is_object($this->pdo)) {
            throw new \Exception('Database connection was not set');
        }

        return $this->pdo;
    }

    /**
     * @param string $queueName
     * @return int
     */
    public function getCountMessages(string $queueName): int
    {
        $sql = 'SELECT COUNT(message_id) FROM message WHERE queue_name = :queue_name';
        $stmt = $this->getDb()->prepare($sql);
        $stmt->execute(array('queue_name' => $queueName));

        return (int)$stmt->fetchColumn();
    }

    /**
     * @param string $queueName
     * @param MessageInterface $message
     * @param string $log
     * @return bool
     * @throws \Exception
     */
    public function add(string $queueName, MessageInterface $message, string $log = ''): bool
    {
        $body = base64_encode(serialize($message));
        $md5 = md5($body);

        try {
            $sql = 'INSERT INTO message
            (queue_name, body, created, md5, log)
            VALUES
            (:queue_name, :body, :created, :md5, :log)
            ';
            $stmt = $this->getDb()->prepare($sql);
            $stmt->bindParam(':queue_name', $queueName, \PDO::PARAM_STR);
            $stmt->bindParam(':body', $body, \PDO::PARAM_STR);
            $stmt->bindParam(':md5', $md5, \PDO::PARAM_STR);
            $stmt->bindValue(':created', time(), \PDO::PARAM_INT);
            $stmt->bindValue(':log', $log, \PDO::PARAM_STR);
            $stmt->execute();
        } catch (\PDOException $e) {
            if ($e->getCode() == 23000) {
                return false;
            } else {
                throw  $e;
            }
        }

        return true;
    }

    /**
     * @param string $queueName
     * @param int $max
     * @return \Generator|MessageInterface[]
     */
    public function getMessages(string $queueName, int $max = 0):\Generator
    {
        if ($max === 0) {
            $max = PHP_INT_MAX;
        }
        $db = $this->getDb();

        // start transaction handling
        if ($max > 0) {
            $sql = "SELECT *
                        FROM message
                        WHERE queue_name = :queue_name
                        AND (handle IS NULL)
                        LIMIT ".$max;
            $stmt = $db->prepare($sql);
            $stmt->execute(array('queue_name' => $queueName));

            while ($data = $stmt->fetch()) {
                $message = unserialize(base64_decode($data['body']));
                if ($message instanceof MessageInterface) {
                    $message->setId((int)$data['message_id']);
                    yield $message;
                }
            }
        }
    }

    /**
     * Remove message from queue
     *
     * @param MessageInterface $message
     * @return bool
     */
    public function deleteMessage(MessageInterface $message): bool
    {
        $sql = 'DELETE FROM message WHERE message_id = ?';
        $stmt = $this->getDb()->prepare($sql);
        $stmt->execute(array($message->getId()));

        return true;
    }

    /**
     * @return QueueManager
     */
    protected function getBaseQueue(): QueueManager
    {
        $dbh = new \PDO('sqlite:'.$this->sqliteDb);
        $dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $dbh->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
        $dbh->exec(
            '
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS message
(
  message_id INTEGER PRIMARY KEY AUTOINCREMENT,
  queue_name VARCHAR(8192),
  handle CHAR(32),
  body VARCHAR(8192) NOT NULL,
  md5 CHAR(32) NOT NULL,
  created INTEGER
, "log" TEXT);
COMMIT;
CREATE UNIQUE INDEX IF NOT EXISTS message_md5 ON message(md5); 
'
        );
        $this->setPdo($dbh);

        return $this;
    }

    /**
     * @param \PDO $pdo
     */
    protected function setPdo(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }
}
