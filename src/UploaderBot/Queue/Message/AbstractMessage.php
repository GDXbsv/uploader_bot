<?php
declare(strict_types = 1);

namespace UploaderBot\Queue\Message;

abstract class AbstractMessage implements MessageInterface
{
    /** @var  int */
    private $id;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return MessageInterface
     */
    public function setId(int $id): MessageInterface
    {
        $this->id = $id;
        
        return $this;
    }
}
