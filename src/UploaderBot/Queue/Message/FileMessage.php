<?php
declare(strict_types = 1);

namespace UploaderBot\Queue\Message;


class FileMessage extends AbstractMessage
{
    /**
     * @var string
     */
    private $path;

    /**
     * ResizeMessage constructor.
     * @param string $path
     */
    public function __construct(string $path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }
}
