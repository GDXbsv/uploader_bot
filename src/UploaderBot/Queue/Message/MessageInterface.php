<?php
declare(strict_types = 1);

namespace UploaderBot\Queue\Message;

interface MessageInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     * @return MessageInterface
     */
    public function setId(int $id): MessageInterface;
}
