<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UploaderBot\Image\Upload\UploadManager;
use UploaderBot\Queue\Message\FileMessage;
use UploaderBot\Queues;

class UploadCommand extends AbstractQueueCommand
{
    /** @var  UploadManager */
    private $uploadManager;
    /** @var  string */
    private $storageName;

    public function setUploadManager(UploadManager $uploadManager)
    {
        $this->uploadManager = $uploadManager;
    }

    public function setStorageName(string $storageName)
    {
        $this->storageName = $storageName;
    }

    protected function configure()
    {
        $this
            ->setName('upload')
            ->setDescription('Upload next images to remote storage')
            ->setProcessTitle('Images Processor Bot Upload')
            ->addOption('number', 'n', InputOption::VALUE_OPTIONAL, 'count of images', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = (int)$input->getOption('number');
        $queueManager = $this->getQueueManager();
        $countSuccess = 0;
        $countFail = 0;
        /** @var FileMessage $message */
        foreach ($queueManager->getMessages(Queues::UPLOAD, $count) as $message) {
            try {
                $this->getQueueManager()->getDb()->beginTransaction();
                try {
                    $isUpload = $this->uploadManager->upload($this->storageName, new \SplFileInfo($message->getPath()));
                    $this->getQueueManager()->deleteMessage($message);
                    $this->getQueueManager()->add(Queues::DONE, new FileMessage($message->getPath()));
                    ++$countSuccess;
                } catch (\Exception $e) {
                    $this->getQueueManager()->deleteMessage($message);
                    $this->getQueueManager()->add(Queues::FAILED, $message);
                    ++$countFail;
                }
                $this->getQueueManager()->getDb()->commit();

            } catch (\Exception $e) {
                $this->getQueueManager()->getDb()->rollBack();
                throw $e;
            }
        }

        $output->writeln(sprintf('<info>Success %d messages</info>', $countSuccess));
        if ($countFail > 0) {
            $output->writeln(sprintf('<error>Fail %d messages</error>', $countFail));
        }
    }
}
