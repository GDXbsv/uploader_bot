<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UploaderBot\Queue\Message\FileMessage;
use UploaderBot\Queues;

class RetryCommand extends AbstractQueueCommand
{
    protected function configure()
    {
        $this
            ->setName('retry')
            ->setDescription('Moves all URLs from failed queue back to resize queue.')
            ->setProcessTitle('Images Processor Bot retry')
            ->addOption('number', 'n', InputOption::VALUE_OPTIONAL, 'count of images', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = (int)$input->getOption('number');
        $queueManager = $this->getQueueManager();
        $i = 0;
        /** @var FileMessage $message */
        foreach ($queueManager->getMessages(Queues::FAILED, $count) as $message) {
            try {
                $this->getQueueManager()->getDb()->beginTransaction();

                $this->getQueueManager()->deleteMessage($message);
                $this->getQueueManager()->add(Queues::RESIZE, $message);

                $this->getQueueManager()->getDb()->commit();
                ++$i;

            } catch (\Exception $e) {
                $this->getQueueManager()->getDb()->rollBack();
                throw $e;
            }

        }

        $output->writeln(sprintf('<info>Success with %d messages</info>', $i));
    }
}
