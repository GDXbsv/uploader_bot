<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UploaderBot\Queues;

class StatusCommand extends AbstractQueueCommand
{
    protected function configure()
    {
        $this
            ->setName('status')
            ->setDescription('Output current status in format %queue%:%number_of_images%')
            ->setProcessTitle('Images Processor Bot');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<comment>Images Processor Bot</comment>');
        $queueManager=$this->getQueueManager();
        $table = new Table($output);
        $table->setStyle('compact');
        $table
            ->setHeaders(array('Queue', 'Count'))
            ->setRows(array(
                [Queues::RESIZE, $queueManager->getCountMessages(Queues::RESIZE)],
                [Queues::UPLOAD, $queueManager->getCountMessages(Queues::UPLOAD)],
                [Queues::DONE, $queueManager->getCountMessages(Queues::DONE)],
                [Queues::FAILED, $queueManager->getCountMessages(Queues::FAILED)],
            ))
        ;
        $table->render();
    }
}
