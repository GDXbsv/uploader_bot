<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Command\Command;
use UploaderBot\Queue\QueueManager;

abstract class AbstractQueueCommand extends Command
{
    /** @var  QueueManager */
    private $queueManager;

    /**
     * @return QueueManager
     */
    public function getQueueManager(): QueueManager
    {
        return $this->queueManager;
    }

    /**
     * @param QueueManager $queueManager
     * @return QueueManager
     */
    public function setQueueManager(QueueManager $queueManager): AbstractQueueCommand
    {
        $this->queueManager = $queueManager;
        return $this;
    }

}
