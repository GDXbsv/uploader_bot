<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use UploaderBot\Queue\Message\FileMessage;
use UploaderBot\Queues;

class ScheduleCommand extends AbstractQueueCommand
{
    protected function configure()
    {
        $this
            ->setName('schedule')
            ->setDescription('Add filenames to resize queue.')
            ->setProcessTitle('Images Processor Bot Schedule')
            ->addArgument('path', InputArgument::REQUIRED, 'a path to the directory with images and schedule them for resize');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        if ($path[0] !== '/') {
            $path = getcwd() . '/' . $path;
        }
        $queueManager = $this->getQueueManager();
        $countFiles = 0;
        $countDoubleFiles = 0;
        foreach (new \DirectoryIterator($path) as $fileInfo) {
            if ($fileInfo->isDot() || !$fileInfo->isFile()) {
                continue;
            }
            $message = new FileMessage($fileInfo->getRealPath());
            if ($queueManager->add(Queues::RESIZE, $message)){
                ++$countFiles;
            } else {
                ++$countDoubleFiles;
            }
        }

        $output->writeln(sprintf('<info>Success %d files</info>', $countFiles));
        $output->writeln(sprintf('<comment>Doubles %d files</comment>', $countDoubleFiles));
    }
}
