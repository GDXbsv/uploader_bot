<?php
declare(strict_types = 1);

namespace UploaderBot\Console\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UploaderBot\Image\Resize\ResizeManager;
use UploaderBot\Queue\Message\FileMessage;
use UploaderBot\Queues;

class ResizeCommand extends AbstractQueueCommand
{
    /** @var  ResizeManager */
    private $resizeManager;

    public function setResizeManager(ResizeManager $resizeManager)
    {
        $this->resizeManager = $resizeManager;
    }

    protected function configure()
    {
        $this
            ->setName('resize')
            ->setDescription('Resize next images from the queue')
            ->setProcessTitle('Images Processor Bot Resize')
            ->addOption('number', 'n', InputOption::VALUE_OPTIONAL, 'count of images', 0);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $count = (int)$input->getOption('number');
        $queueManager = $this->getQueueManager();
        $countSuccess = 0;
        $countFail = 0;
        /** @var FileMessage $message */
        foreach ($queueManager->getMessages(Queues::RESIZE, $count) as $message) {
            try {
                $this->getQueueManager()->getDb()->beginTransaction();
                try {
                    $imageResultInfo = $this->resizeManager->resizeAndDrop(new \SplFileInfo($message->getPath()));
                    $this->getQueueManager()->deleteMessage($message);
                    $this->getQueueManager()->add(Queues::UPLOAD, new FileMessage($imageResultInfo->getRealPath()));
                    ++$countSuccess;
                } catch (\Exception $e) {
                    $output->writeln(sprintf('<error>Error: %s</error>', $e->getMessage()));
                    $this->getQueueManager()->deleteMessage($message);
                    $this->getQueueManager()->add(Queues::FAILED, $message);
                    ++$countFail;
                }
                $this->getQueueManager()->getDb()->commit();

            } catch (\Exception $e) {
                $this->getQueueManager()->getDb()->rollBack();
                throw $e;
            }

        }

        $output->writeln(sprintf('<info>Success %d messages</info>', $countSuccess));
        if ($countFail > 0) {
            $output->writeln(sprintf('<error>Fail %d messages</error>', $countFail));
        }
    }
}
