<?php
declare(strict_types = 1);

namespace UploaderBot\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Dumper\PhpDumper;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use UploaderBot\DependencyInjection\CompilerPass\CommandCompilerPass;
use UploaderBot\DependencyInjection\CompilerPass\StorageCompilerPass;

class Configuration
{
    /** @var Container */
    private $container;

    public function __construct()
    {
        $file = __DIR__.'/../../../cache/container.php';

        if (file_exists($file)) {
            require_once $file;
            $container = new \ProjectServiceContainer();
        } else {
            $parameterBag = new ParameterBag($this->arrayDot(require __DIR__.'/../../../config/config.php'));
            $container = new ContainerBuilder($parameterBag);
            $loader = new XmlFileLoader($container, new FileLocator(__DIR__));
            $loader->load('services.xml');
            $container->addCompilerPass(new CommandCompilerPass());
            $container->addCompilerPass(new StorageCompilerPass());
            $container->compile();

            $dumper = new PhpDumper($container);
            file_put_contents($file, $dumper->dump());
        }

        $this->container = $container;
    }

    public function getContainer(): Container
    {
        return $this->container;
    }

    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array $array
     * @param  string $prepend
     * @return array
     */
    protected function arrayDot($array, $prepend = '')
    {
        $results = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $results = array_merge($results, $this->dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }

    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param  array $array
     * @param  string $prepend
     * @return array
     */
    protected function dot($array, $prepend = '')
    {
        $results = array();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $results = array_merge($results, $this->dot($value, $prepend.$key.'.'));
            } else {
                $results[$prepend.$key] = $value;
            }
        }

        return $results;
    }

}
