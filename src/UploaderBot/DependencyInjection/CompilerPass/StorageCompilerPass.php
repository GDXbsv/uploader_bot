<?php
declare(strict_types = 1);

namespace UploaderBot\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class StorageCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('storage_chain')) {
            return;
        }

        $definition = $container->findDefinition(
            'storage_chain'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'storage_chain'
        );
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'addStorage',
                array($tags[0]['alias'], new Reference($id))
            );
        }
    }
}
