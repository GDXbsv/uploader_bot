<?php
declare(strict_types = 1);

namespace UploaderBot\DependencyInjection\CompilerPass;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class CommandCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('application')) {
            return;
        }

        $definition = $container->findDefinition(
            'application'
        );

        $taggedServices = $container->findTaggedServiceIds(
            'command'
        );
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall(
                'add',
                array(new Reference($id))
            );
        }
    }
}
