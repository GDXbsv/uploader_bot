<?php
declare(strict_types = 1);

namespace UploaderBot;

class Queues
{
    const RESIZE = 'resize';
    const UPLOAD = 'upload';
    const DONE = 'done';
    const FAILED = 'failed';
}
